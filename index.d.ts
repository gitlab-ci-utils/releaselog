export type ReleaseDetails = {
  /**
   * The title of the release from the CHANGELOG.
   */
  title: string;

  /**
   * The release notes for the release from the CHANGELOG.
   */
  notes: string;
};

/**
 * Finds the "primary" changelog in the specified location. The changelog name must contain
 * "changelog" (case-insensitive). For example `CHANGELOG`, `changelog.md`, `CHANGELOG-EE.md`.
 * If multiple changelogs are found in that directory, `changelog.md` (case-insensitive) is
 * assumed to be primary.  Otherwise, the first changelog in the directory listing is used.
 *
 * @param   location The directory to be checked for changelogs.
 * @returns          The full path of the changelog in the specified location,
 *                   or undefined if none found.
 * @static
 */
export function findChangelog(location: string): string | undefined;

/**
 * Gets release details for the given version in the specified changelog.
 *
 * @param   changelog The location of the changelog file to be checked.
 * @param   version   The version to find in the changelog.
 * @returns           Object with the title and releaseNotes for the given version,
 *                    if found, otherwise undefined.
 * @static
 */
export function getReleaseDetails(
  changelog: string,
  version: string
): ReleaseDetails | undefined;
