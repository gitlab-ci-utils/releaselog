'use strict';

/**
 * Miscellaneous functions used by commander for CLI input validation.
 * All functions log error and exit the process if invalid.
 *
 * @module validate-utils
 */

const fs = require('node:fs');
const logger = require('ci-logger');
const releaselog = require('..');

const getErrorLogEntry = (message) => ({
    errorCode: 1,
    exitOnError: true,
    level: logger.Levels.Error,
    message
});

/**
 * Validate that the specified CHANGELOG value is a valid file or directory.
 * If it is a valid file, the file name is returned. If it is a valid
 * directory it is searched for CHANGELOGs and one is returned
 * ([see findChangelog]{@link releaselog#findChangelog}). Logs error and exits process if file name is not specified, file does not exist, or CHANGELOG cannot be found in the specified directory.
 *
 * @param   {string} value The specified CHANGELOG file or directory.
 * @returns {string}       The CHANGELOG file name.
 * @static
 */
const validateChangelogValue = (value) => {
    // Validate file or directory exists before checking type
    // Opens file per user-specific directory, no limits on location.
    // nosemgrep: detect-non-literal-fs-filename, eslint.detect-non-literal-fs-filename
    if (!fs.existsSync(value)) {
        logger.log(
            getErrorLogEntry(
                `Invalid changelog "${value}", must be a valid file or directory`
            )
        );
    }
    // Pull stats to confirm type.
    // Opens file per user-specific directory, no limits on location.
    // nosemgrep: detect-non-literal-fs-filename, eslint.detect-non-literal-fs-filename
    const stats = fs.lstatSync(value);
    if (stats.isDirectory()) {
        // If a directory then find a CHANGELOG
        const changelog = releaselog.findChangelog(value);
        if (changelog) {
            return changelog;
        }
        logger.log(
            getErrorLogEntry(`No changelog was found in directory "${value}"`)
        );
    }
    // Value is a valid file
    return value;
};

/**
 * Validate that the specified format has a valid value ("json", "title", or "notes").
 * Logs error and exits process if format is invalid.
 *
 * @param   {string} value The specified format value.
 * @returns {string}       The original value, if valid.
 * @static
 */
const validateFormatValue = (value) => {
    const validFormats = ['json', 'title', 'notes'];
    if (!validFormats.includes(value)) {
        logger.log(
            getErrorLogEntry(
                `Invalid format "${value}", must be one of "${validFormats.join(
                    '", "'
                )}"`
            )
        );
    }
    return value;
};

/**
 * Validate that a release version is specified. Logs error and
 * exits process if release version is not specified.
 *
 * @param   {string} version The specified version.
 * @returns {string}         The original value, if valid.
 * @static
 */
const validateReleaseVersion = (version) => {
    if (!version) {
        logger.log(getErrorLogEntry('Version must be specified'));
    }
    return version;
};

module.exports = {
    validateChangelogValue,
    validateFormatValue,
    validateReleaseVersion
};
