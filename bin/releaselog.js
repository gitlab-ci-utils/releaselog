#!/usr/bin/env node
'use strict';

const logger = require('ci-logger');
const { program } = require('commander');
const {
    validateChangelogValue,
    validateFormatValue,
    validateReleaseVersion
} = require('../lib/validate-utils');
const releaselog = require('..');

const processArguments = () => {
    program
        .usage('[options] <release-version>')
        .option(
            '-c, --changelog <file>',
            'the path to the CHANGELOG (directory or file)',
            './'
        )
        .option(
            '-f, --format <value>',
            'the output format ("json", "title", or "notes")',
            validateFormatValue,
            'json'
        )
        .allowExcessArguments()
        .parse(process.argv);
    const options = program.opts();
    // ValidateChangelogValue called here so default value is processed as well
    options.changelog = validateChangelogValue(options.changelog);
    options.releaseVersion = validateReleaseVersion(program.args[0]);
    return options;
};

// eslint-disable-next-line max-lines-per-function -- exceeded by formatting
const getRelease = () => {
    const options = processArguments();
    const release = releaselog.getReleaseDetails(
        options.changelog,
        options.releaseVersion
    );

    if (release) {
        /* eslint-disable-next-line default-case -- Format is confirmed
           above to be a valid value, so default case is not required */
        switch (options.format) {
            case 'json': {
                console.log(JSON.stringify(release));
                break;
            }
            case 'title': {
                console.log(release.title);
                break;
            }
            case 'notes': {
                console.log(release.notes);
                break;
            }
        }
    } else {
        const message = `No release found for version "${options.releaseVersion}" in changelog "${options.changelog}"`;
        logger.log({
            errorCode: 1,
            exitOnError: true,
            level: logger.Levels.Error,
            message
        });
    }
};

getRelease();
