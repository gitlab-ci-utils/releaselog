import { expectError, expectType } from 'tsd';
import { findChangelog, getReleaseDetails, ReleaseDetails } from '.';

// findChangelog
// Test valid inputs
const result = findChangelog('./');

// Test invalid inputs
expectError(findChangelog());
expectError(findChangelog(true));
expectError(findChangelog({ location: './' }));

// Test return types
expectType<string | undefined>(findChangelog('./'));
expectType<string | undefined>(findChangelog('./tests'));

// getReleaseDetails
// Test valid inputs
const changelogLocation = result ? (result as string) : './CHANGELOG.md';
const version = '1.0.0';
const release = getReleaseDetails(changelogLocation, version);

// Test invalid inputs
expectError(getReleaseDetails());
expectError(getReleaseDetails({}, version));
expectError(getReleaseDetails(true, version));
expectError(getReleaseDetails(changelogLocation, true));
expectError(getReleaseDetails(changelogLocation, { version }));

// Test return types
expectType<ReleaseDetails | undefined>(
  getReleaseDetails(changelogLocation, version)
);
expectType<ReleaseDetails | undefined>(getReleaseDetails('./tests', version));
expectType<ReleaseDetails | undefined>(
  getReleaseDetails(changelogLocation, '100.100.100')
);
if (release) {
  expectType<string>(release.title);
  expectType<string>(release.notes);
}
