'use strict';

const path = require('node:path');
const releaselog = require('../');
const changelogTestCases = require('./changelog-fixtures');

const invalidLocationMessage = 'location must be specified';
const locationNotFoundMessage = 'ENOENT';
const invalidChangelogLocationMessage = 'changelog must be specified';
const invalidVersionMessage = 'version must be specified';

describe('find', () => {
    describe('validate input', () => {
        const testThrowsError = (location, error) => {
            expect(() => releaselog.findChangelog(location)).toThrow(error);
        };

        it('should throw an error if location is not specified', () => {
            expect.assertions(1);
            testThrowsError(undefined, invalidLocationMessage);
        });

        it('should throw an error if location is empty string', () => {
            expect.assertions(1);
            testThrowsError('', invalidLocationMessage);
        });

        it('should throw an error if location is invalid', () => {
            expect.assertions(1);
            testThrowsError(0, invalidLocationMessage);
        });

        it('should throw an error if location is not found', () => {
            expect.assertions(1);
            testThrowsError('./somewhere', locationNotFoundMessage);
        });
    });

    describe('return correct changelog from test cases', () => {
        afterEach(() => {
            jest.restoreAllMocks();
        });

        const testChangelogCases = (directory, filename) => {
            expect(releaselog.findChangelog(directory)).toBe(
                path.join(directory, filename)
            );
        };

        it('should return undefined if no changelogs', () => {
            expect.assertions(1);
            expect(
                releaselog.findChangelog('./tests/fixtures/case1')
            ).toBeUndefined();
        });

        it('should return the correct changelog if one file with no extension', () => {
            expect.assertions(1);
            testChangelogCases('./tests/fixtures/case2', 'CHANGELOG');
        });

        it('should return the correct changelog if the default file', () => {
            expect.assertions(1);
            testChangelogCases('./tests/fixtures/case3', 'CHANGELOG.md');
        });

        it('should return the correct changelog if multiple files and one is the default file', () => {
            expect.assertions(1);
            testChangelogCases('./tests/fixtures/case4', 'CHANGELOG.md');
        });

        it('should return the correct changelog if multiple files and none are the default file', () => {
            expect.assertions(1);
            testChangelogCases('./tests/fixtures/case5', 'CHANGELOG-FREE.md');
        });
    });
});

describe('getReleaseDetails', () => {
    describe('validate input', () => {
        afterEach(() => {
            jest.restoreAllMocks();
        });

        const testThrowsError = (changelog, version, error) => {
            expect(() =>
                releaselog.getReleaseDetails(changelog, version)
            ).toThrow(error);
        };

        it('should throw an error if changelog is not specified', () => {
            expect.assertions(1);
            testThrowsError(
                undefined,
                changelogTestCases.case2.version,
                invalidChangelogLocationMessage
            );
        });

        it('should throw an error if changelog is empty string', () => {
            expect.assertions(1);
            testThrowsError(
                '',
                changelogTestCases.case2.version,
                invalidChangelogLocationMessage
            );
        });

        it('should throw an error if changelog is invalid', () => {
            expect.assertions(1);
            testThrowsError(
                0,
                changelogTestCases.case2.version,
                invalidChangelogLocationMessage
            );
        });

        it('should throw an error if changelog is not found', () => {
            expect.assertions(1);
            testThrowsError(
                './somewhere',
                changelogTestCases.case2.version,
                locationNotFoundMessage
            );
        });

        it('should throw an error if version is not specified', () => {
            expect.assertions(1);
            testThrowsError(
                changelogTestCases.case2.changelog,
                undefined,
                invalidVersionMessage
            );
        });

        it('should throw an error if version is empty string', () => {
            expect.assertions(1);
            testThrowsError(
                changelogTestCases.case2.changelog,
                '',
                invalidVersionMessage
            );
        });
    });

    describe('release results', () => {
        afterEach(() => {
            jest.restoreAllMocks();
        });

        const checkChangelogSnapshot = (changelog, version) => {
            expect(
                releaselog.getReleaseDetails(changelog, version)
            ).toMatchSnapshot();
        };

        it('should return the proper release notes for semver with ## headers', () => {
            expect.assertions(1);
            checkChangelogSnapshot(
                changelogTestCases.case2.changelog,
                changelogTestCases.case2.version
            );
        });

        it('should return the proper release notes for semver with ### headers', () => {
            expect.assertions(1);
            checkChangelogSnapshot(
                changelogTestCases.case3.changelog,
                changelogTestCases.case3.version
            );
        });

        it('should return the proper release notes for semver with #### headers', () => {
            expect.assertions(1);
            checkChangelogSnapshot(
                changelogTestCases.case4.changelog,
                changelogTestCases.case4.version
            );
        });

        it('should return the proper release notes for semver with ##### headers', () => {
            expect.assertions(1);
            checkChangelogSnapshot(
                changelogTestCases.case5.changelog,
                changelogTestCases.case5.version
            );
        });

        it('should return the proper release notes for calver with YY.0M.MICRO format', () => {
            expect.assertions(1);
            checkChangelogSnapshot(
                changelogTestCases.case6.changelog,
                changelogTestCases.case6.versionCalver_YY_0M_MICRO
            );
        });

        it('should return the proper release notes for calver with YYYY.0M.0D format', () => {
            expect.assertions(1);
            checkChangelogSnapshot(
                changelogTestCases.case6.changelog,
                changelogTestCases.case6.versionCalver_YYYY_0M_0D
            );
        });

        it('should return the proper release notes for calver with YYYY.MM format', () => {
            expect.assertions(1);
            checkChangelogSnapshot(
                changelogTestCases.case6.changelog,
                changelogTestCases.case6.versionCalver_YYYY_MM
            );
        });

        it('should return the proper release notes with version in non-header text of a later release', () => {
            expect.assertions(1);
            checkChangelogSnapshot(
                changelogTestCases.case4.changelogPro,
                changelogTestCases.case4.versionPro
            );
        });

        it('should return the same release notes with and without version prefixed with "v"', () => {
            expect.assertions(1);
            expect(
                releaselog.getReleaseDetails(
                    changelogTestCases.case2.changelog,
                    changelogTestCases.case2.version
                )
            ).toStrictEqual(
                releaselog.getReleaseDetails(
                    changelogTestCases.case2.changelog,
                    `v${changelogTestCases.case2.version}`
                )
            );
        });

        it('should return undefined if the version is not found', () => {
            expect.assertions(1);
            expect(
                releaselog.getReleaseDetails(
                    changelogTestCases.case2.changelog,
                    changelogTestCases.case2.badVersion
                )
            ).toBeUndefined();
        });

        it('should escape tag string for regex before checking changelog for release', () => {
            expect.assertions(1);
            // If tag is not properly escaped for regex then will return a different changelog title
            expect(
                releaselog.getReleaseDetails(
                    changelogTestCases.case7.changelog,
                    changelogTestCases.case7.version
                ).title
            ).toStrictEqual(changelogTestCases.case7.expectedResult);
        });
    });
});
