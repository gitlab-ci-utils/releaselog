'use strict';

module.exports = {
    case2: {
        badVersion: '2.3.4',
        changelog: 'tests/fixtures/case2/CHANGELOG',
        version: '1.1.0'
    },
    case3: {
        changelog: 'tests/fixtures/case3/CHANGELOG.md',
        version: '2.1.0'
    },
    case4: {
        alternateVersion: '3.1.1',
        changelog: 'tests/fixtures/case4/CHANGELOG.md',
        changelogPro: 'tests/fixtures/case4/CHANGELOG-PRO.md',
        version: '3.1.0',
        versionPro: '1.0.0'
    },
    case5: {
        changelog: 'tests/fixtures/case5/CHANGELOG-FREE.md',
        version: '4.1.0'
    },
    case6: {
        changelog: 'tests/fixtures/case5/CHANGELOG-PRO.md',
        /* eslint-disable-next-line camelcase -- required to describe format */
        versionCalver_YY_0M_MICRO: '19.3.1',
        /* eslint-disable-next-line camelcase -- required to describe format */
        versionCalver_YYYY_0M_0D: '2019.3.6',
        /* eslint-disable-next-line camelcase -- required to describe format */
        versionCalver_YYYY_MM: '2019.03'
    },
    case7: {
        changelog: 'tests/fixtures/case7/CHANGELOG.md',
        expectedResult: 'Version 0.1.0 (2019-02-03)',
        version: '0.1.0'
    }
};
