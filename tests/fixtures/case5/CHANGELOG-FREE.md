Changelog
==========

##### Version 4.1.1 (2019-03-06)

**Added**

- Add new feature 2
- Add yet another new feature

**Fixed**

- Really update `foo` to fix `bar`


##### Version 4.1.0 (2019-02-03)

**Added**

- Add new feature 1
- Add another new feature

**Fixed**

- Update `foo` to fix `bar`


##### Version 4.0.0 (2019-01-26)

- Initial release
