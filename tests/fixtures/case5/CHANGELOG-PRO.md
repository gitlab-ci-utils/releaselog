Changelog
==========

## Version 19.3.1

### Added

- Add new feature 5
- Add new _feature_ 6

### Fixed

- Change `foo` to `FOO`


## Version 2019.3.6

### Added

- Add new feature 3
- Add yet another new feature

### Fixed

- Change `bar` back to `foo`


## Version 2019.03

### Added

- Add new feature 1
- Add another new feature

### Fixed

- Update `foo` to fix `bar`


## Version 1.0.0 (2019-01-26)

- Initial release
