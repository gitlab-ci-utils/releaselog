Changelog
==========

## Version 1.1.0 (2019-02-03)

### Added

- Add new feature to 1.0.0
- Add another new feature

### Fixed

- Update `foo` to fix `bar`


## Version 1.0.0 (2019-01-26)

- Initial release
