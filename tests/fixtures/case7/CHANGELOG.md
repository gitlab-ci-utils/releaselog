# Changelog

## Version 1.0.2 (2021-02-27)

### Added

- Add new feature 2
- Add yet another new feature

### Fixed

- Really update `foo` to fix `bar`


## Version 0.1.0 (2019-02-03)

### Added

- Add new feature 1
- Add another new feature

### Fixed

- Update `foo` to fix `bar`


## Version 1.0.0 (2019-01-26)

- Initial release
