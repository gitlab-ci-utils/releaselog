'use strict';

const path = require('node:path');
const { bin } = require('bin-tester');
const changelogTestCases = require('./changelog-fixtures');

const defaultTestCase = changelogTestCases.case3;

describe('cli', () => {
    it('should return expected results if changelog, format and version specified', async () => {
        expect.assertions(3);
        const binArguments = [
            '-c',
            changelogTestCases.case3.changelog,
            '-f',
            'json',
            changelogTestCases.case3.version
        ];

        const results = await bin({ binArguments });

        expect(results.code).toBe(0);
        expect(results.stderr).toBe('');
        expect(results.stdout).toMatchSnapshot();
    });

    describe('version', () => {
        const validateExpectedError = async (binArguments, errorMessage) => {
            expect.assertions(3);
            const results = await bin({ binArguments });

            expect(results.code).toBe(1);
            expect(results.stdout).toBe('');
            expect(results.stderr).toMatch(errorMessage);
        };

        it('should generate expected error if no version specified', async () => {
            expect.hasAssertions();
            await validateExpectedError([], 'Version');
        });

        it('should generate expected error if release is not found in changelog', async () => {
            expect.hasAssertions();
            const invalidRelease = '1.1.1';
            await validateExpectedError([invalidRelease], 'No release found');
        });
    });

    describe('format option', () => {
        const validateFormatConsoleOutput = async (format, option = '-f') => {
            expect.assertions(3);
            const options = {
                binArguments: [option, format, defaultTestCase.version],
                workingDirectory: path.dirname(defaultTestCase.changelog)
            };

            const results = await bin(options);

            expect(results.code).toBe(0);
            expect(results.stderr).toBe('');
            expect(results.stdout).toMatchSnapshot();
        };

        it('should write release json to console if "json" format is specified with -f', async () => {
            expect.hasAssertions();
            await validateFormatConsoleOutput('json');
        });

        it('should write release title to console if "title"format is specified with -f', async () => {
            expect.hasAssertions();
            await validateFormatConsoleOutput('title');
        });

        it('should write release notes to console if "notes" format is specified with -f', async () => {
            expect.hasAssertions();
            await validateFormatConsoleOutput('notes');
        });

        it('should write release json to console if "json" format is specified with --format', async () => {
            expect.hasAssertions();
            await validateFormatConsoleOutput('json', '--format');
        });
    });

    describe('changelog option', () => {
        const alternateTestCase = changelogTestCases.case4;

        const validateChangelogConsoleOutput = async (binArguments, cwd) => {
            expect.assertions(3);
            const options = {
                binArguments,
                workingDirectory: path.dirname(cwd)
            };

            const results = await bin(options);

            expect(results.code).toBe(0);
            expect(results.stderr).toBe('');
            expect(results.stdout).toMatchSnapshot();
        };

        it('should open CHANGELOG in current working directory if none specified', async () => {
            expect.hasAssertions();
            const binArguments = [alternateTestCase.alternateVersion];
            await validateChangelogConsoleOutput(
                binArguments,
                alternateTestCase.changelog
            );
        });

        it('should open specific changelog if specified with -c', async () => {
            expect.hasAssertions();
            const binArguments = [
                '-c',
                path.resolve(defaultTestCase.changelog),
                defaultTestCase.version
            ];
            await validateChangelogConsoleOutput(
                binArguments,
                alternateTestCase.changelog
            );
        });

        it('should open specific changelog if specified with --changelog', async () => {
            expect.hasAssertions();
            const binArguments = [
                '--changelog',
                path.resolve(defaultTestCase.changelog),
                defaultTestCase.version
            ];
            await validateChangelogConsoleOutput(
                binArguments,
                alternateTestCase.changelog
            );
        });
    });
});
