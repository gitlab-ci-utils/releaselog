'use strict';

const fs = require('node:fs');
const logger = require('ci-logger');
const releaselogCli = require('../lib/validate-utils');
const releaselog = require('..');

const getErrorLogObject = (message) => ({
    exitOnError: true,
    level: logger.Levels.Error,
    message: expect.stringContaining(message)
});

const verifyLogErrorAndExit = (value, testFunction, errorMessage) => {
    const loggerSpy = jest.spyOn(logger, 'log').mockImplementation(() => {});

    testFunction(value);

    expect(loggerSpy).toHaveBeenCalledWith(
        expect.objectContaining(getErrorLogObject(errorMessage))
    );
};

describe('validate input', () => {
    afterEach(() => {
        jest.restoreAllMocks();
    });

    describe('version', () => {
        it('should return the original version if passed a value', () => {
            expect.assertions(1);
            const version = '1.1.1';
            expect(releaselogCli.validateReleaseVersion(version)).toStrictEqual(
                version
            );
        });

        it('should log error and exit if version is undefined', () => {
            expect.assertions(1);
            const version = undefined;
            verifyLogErrorAndExit(
                version,
                releaselogCli.validateReleaseVersion,
                'Version'
            );
        });

        it('should log error and exit if version is empty string', () => {
            expect.assertions(1);
            const version = '';
            verifyLogErrorAndExit(
                version,
                releaselogCli.validateReleaseVersion,
                'Version'
            );
        });
    });

    describe('format', () => {
        // Commander configuration requires format to have a value, so no tests
        // required for no value cases (undefined, empty string, etc)

        it('should return value if valid value "json" provided', () => {
            expect.assertions(1);
            const format = 'json';
            expect(releaselogCli.validateFormatValue(format)).toStrictEqual(
                format
            );
        });

        it('should return value if valid value "title" provided', () => {
            expect.assertions(1);
            const format = 'title';
            expect(releaselogCli.validateFormatValue(format)).toStrictEqual(
                format
            );
        });

        it('should return value if valid value "notes" provided', () => {
            expect.assertions(1);
            const format = 'notes';
            expect(releaselogCli.validateFormatValue(format)).toStrictEqual(
                format
            );
        });

        it('should log error and exit if format is an invalid value', () => {
            expect.assertions(1);
            const format = 'text';
            verifyLogErrorAndExit(
                format,
                releaselogCli.validateFormatValue,
                'Invalid format'
            );
        });
    });

    describe('changelog', () => {
        const invalidChangelogError = 'Invalid changelog';

        const verifyChangelogInvalidValue = (changelog, errorMessage) => {
            const isDirectory = () => false;
            const loggerSpy = jest
                .spyOn(logger, 'log')
                .mockImplementation(() => {});

            // The logger would normally exit, but because it is mocked processing continues,
            // so need to mock lstatSync or it will throw.  In this case isDirectory always
            // returns false, which will cause validateChangelogValue to return its input value.
            jest.spyOn(fs, 'lstatSync').mockImplementation(() => ({
                isDirectory
            }));

            releaselogCli.validateChangelogValue(changelog);

            expect(loggerSpy).toHaveBeenCalledWith(
                expect.objectContaining(getErrorLogObject(errorMessage))
            );
        };

        it('should return the original value if changelog is a valid file', () => {
            expect.assertions(1);
            const changelog = 'CHANGELOG.md';
            expect(
                releaselogCli.validateChangelogValue(changelog)
            ).toStrictEqual(changelog);
        });

        it('should call findChangelog and return that result if changelog is a valid directory', () => {
            expect.assertions(2);
            const changelog = './';
            const changelogFile = releaselog.findChangelog(changelog);
            const releaselogSpy = jest.spyOn(releaselog, 'findChangelog');

            const result = releaselogCli.validateChangelogValue(changelog);

            expect(result).toStrictEqual(changelogFile);
            expect(releaselogSpy).toHaveBeenCalledWith(changelog);
        });

        it('should log error and exit if changelog is a valid directory and no changelog file is found', () => {
            expect.assertions(1);
            const changelog = './bin';
            const loggerSpy = jest
                .spyOn(logger, 'log')
                .mockImplementation(() => {});

            releaselogCli.validateChangelogValue(changelog);

            expect(loggerSpy).toHaveBeenCalledWith(
                expect.objectContaining(getErrorLogObject(changelog))
            );
        });

        it('should log error and exit if changelog is undefined', () => {
            expect.assertions(1);
            const changelog = undefined;
            verifyChangelogInvalidValue(changelog, invalidChangelogError);
        });

        it('should log error and exit if changelog is empty string', () => {
            expect.assertions(1);
            const changelog = '';
            verifyChangelogInvalidValue(changelog, invalidChangelogError);
        });

        it('should log error and exit if changelog is an invalid directory path', () => {
            expect.assertions(1);
            const changelog = './foo/';
            verifyChangelogInvalidValue(changelog, invalidChangelogError);
        });

        it('should log error and exit if changelog is an invalid file path', () => {
            expect.assertions(1);
            const changelog = './foo.md';
            verifyChangelogInvalidValue(changelog, invalidChangelogError);
        });
    });
});
