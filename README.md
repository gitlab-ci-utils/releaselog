# Releaselog

Releaselog extracts the title and notes for a specific release from a
CHANGELOG. For details on why to keep a CHANGELOG, CHANGELOG best
practices, etc see https://keepachangelog.com/.

## Installation

```sh
npm install releaselog
```

## Command line usage

The `releaselog` command line interface is run as follows:

```sh
Usage: releaselog [options] <version>

Options:
  -c, --changelog <file>  the path to the CHANGELOG (directory or file) (default: "./")
  -f, --format <value>    the output format ("json", "title", or "notes") (default: "json")
  -h, --help              output usage information
```

The version to extract from the CHANGELOG must be provided. For example, to
output the details for version 1.0.0 from a CHANGELOG in the current
directory to a JSON file (assuming installed globally):

```sh
> releaselog 1.0.0 > release.json

# release.json:
# {"title":"Version 1.0.0","notes":"Initial release"}
```

The `format` option can be used to extract a subset of the release details
(`title` or `notes`). From the previous example, to output only the release
notes for version 1.0.0 to a file:

```sh
> releaselog --format=notes 1.0.0 > release_notes.txt

# release_notes.txt:
# Initial release
```

The optional `changelog` parameter can be used to provide the path to either a
file or directory. If not provided, the current working directory is used. If
any directory is specified, the `findChangelog` function is used to find the
CHANGELOG file. See below for details on how the CHANGELOG file is selected.

## Module usage

The `releaselog` module includes two functions: `getReleaseDetails` and `findChangelog`.

`getReleaseDetails` returns an object with the title and notes for the given
version if found in the specified CHANGELOG.

```js
const releaselog = require('releaselog');
const { title, notes } = releaselog.getReleaseDetails(
  './CHANGELOG.md',
  '1.0.0'
);
```

See below for CHANGELOG requirements for use with `releaselog`.

If not know, `findChangelog` can be used to return the full path of the
CHANGELOG in the specified directory.

```js
const releaselog = require('releaselog');
const changelog = releaselog.findChangelog('./');
const { title, notes } = releaselog.getReleaseDetails(changelog, '1.0.0');
```

This finds the "primary" CHANGELOG in the specified location. The CHANGELOG
name must contain "changelog" (case-insensitive). for example `CHANGELOG`,
`changelog.md`, `CHANGELOG-EE.md`. If multiple CHANGELOGs are found in that
directory, `changelog.md` (case-insensitive) is assumed to be primary.
Otherwise, the first CHANGELOG in the directory listing is used.

## Changelog requirements

Releaselog is designed to support whatever versioning scheme is used -
[SemVer](https://semver.org/), [CalVer](https://calver.org/), etc. It also
allows flexibility in the CHANGELOG format, but it must conform to the
following requirements:

- The file format must be markdown
- Version titles must use markdown headers. The header can be any level ##
  through ##### (not #), but the same header type must be used for all titles.
- The text of the version must be included in the version title line. If the
  version starts with "v" (for example "v1.1.0"), it is stripped and the
  remaining text is used (for example "1.1.0").

Once the current version is found, the header format for that line is assumed
for all versions, and the next occurrence in the CHANGELOG signals the end of
the release content. All text between those lines is included in the release
notes, allowing flexibility in the release note content.

As an example, searching the following CHANGELOG for version "v1.1.0" (note the
"v" is stripped before searching the CHANGELOG):

```md
# Changelog

## Version 1.1.0 (2019-02-03)

### Added

- Add new feature 1
- Add another new feature

### Fixed

- Update `foo` to fix `bar`

## Version 1.0.0 (2019-01-26)

- Initial release
```

Results in a release with the title `Version 1.1.0 (2019-02-03)` and the
following notes:

```md
### Added

- Add new feature 1
- Add another new feature

### Fixed

- Update `foo` to fix `bar`
```
