'use strict';

/**
 * Releaselog extracts the title and notes for a specific release from a CHANGELOG.
 *
 * @module releaselog
 */

const fs = require('node:fs');
const path = require('node:path');

const verifyHasValue = (data, message) => {
    if (!data) {
        throw new TypeError(message);
    }
};

// Logic based on https://www.npmjs.com/package/escape-string-regexp,
// MIT License, Copyright (c) Sindre Sorhus
const escapeStringRegex = (value) =>
    value.replaceAll(/[$()*+.?[\\\]^{|}-]/g, String.raw`\$&`);

/**
 * Finds the "primary" changelog in the specified location. The changelog name must contain
 * "changelog" (case-insensitive). For example `CHANGELOG`, `changelog.md`, `CHANGELOG-EE.md`.
 * If multiple changelogs are found in that directory, `changelog.md` (case-insensitive) is
 * assumed to be primary.  Otherwise, the first changelog in the directory listing is used.
 *
 * @param   {string}           location The directory to be checked for changelogs.
 * @returns {string|undefined}          The full path of the changelog in the specified location,
 *                                      or undefined if none found.
 * @static
 */
const findChangelog = (location) => {
    verifyHasValue(location, 'location must be specified');

    const allChangelogsRegex = /changelog/i;
    const primaryChangelogRegex = /changelog.md/i;
    // Opens file per user-specific directory, no limits on location.
    // nosemgrep: detect-non-literal-fs-filename, eslint.detect-non-literal-fs-filename
    const files = fs.readdirSync(location);

    const changelogs = files.filter((f) => f.match(allChangelogsRegex));
    let changelog;

    if (changelogs.length === 0) {
        return;
    } else if (changelogs.length === 1) {
        changelog = changelogs[0]; // eslint-disable-line prefer-destructuring -- less intuitive
    } else {
        const primaryChangelogs = files.filter((f) =>
            f.match(primaryChangelogRegex)
        );
        changelog =
            primaryChangelogs.length === 1
                ? primaryChangelogs[0]
                : changelogs[0];
    }
    /* eslint-disable consistent-return -- all paths return a value */
    // Opens file per user-specific directory, no limits on location.
    // nosemgrep: path-join-resolve-traversal
    return path.join(location, changelog);
    /* eslint-enable consistent-return -- all paths return a value */
};

/**
 * Gets release details for the given version in the specified changelog.
 *
 * @param   {string}           changelog The location of the changelog file to be checked.
 * @param   {string}           version   The version to find in the changelog.
 * @returns {object|undefined}           Object with the title and releaseNotes for the given version,
 *                                       if found, otherwise undefined.
 * @static
 */
// eslint-disable-next-line consistent-return -- Returns undefined if release not found
const getReleaseDetails = (changelog, version) => {
    verifyHasValue(changelog, 'changelog must be specified');
    verifyHasValue(version, 'version must be specified');

    // Drop 'v' if prefixing version
    const searchVersion = version.startsWith('v') ? version.slice(1) : version;
    // Development tool with user specified input, no ReDoS risk.
    // nosemgrep: detect-non-literal-regexp, eslint.detect-non-literal-regexp
    const re = new RegExp(
        `^(#{2,} )(.*${escapeStringRegex(searchVersion)}.*)$`
    );
    let title;
    let notes = '';
    let delimiter = '##';

    // Split changelog into lines, independent of line endings.
    // Opens file per user-specific directory, no limits on location.
    // nosemgrep: detect-non-literal-fs-filename, eslint.detect-non-literal-fs-filename
    const fileLines = fs.readFileSync(changelog, 'utf8').split(/\r?\n/);
    // Check for a line that matches the release regex
    const releaseStartLine = fileLines.findIndex((line) => line.match(re));
    if (releaseStartLine !== -1) {
        // Get release delimiter and title for release
        [, delimiter, title] = fileLines[releaseStartLine].match(re);

        // Check for the next line matching the release delimiter (start
        // of the next release). If not found, use end of file.
        const nextReleaseLine = fileLines.findIndex(
            (line, index) =>
                index > releaseStartLine && line.startsWith(delimiter)
        );
        const releaseEndLine =
            nextReleaseLine === -1 ? fileLines.length : nextReleaseLine;

        // Pull lines for this release (excluding title) and reformat as string
        notes = fileLines
            .slice(releaseStartLine + 1, releaseEndLine)
            .join('\n')
            .trim();
        return { notes, title };
    }
};

module.exports.findChangelog = findChangelog;
module.exports.getReleaseDetails = getReleaseDetails;
