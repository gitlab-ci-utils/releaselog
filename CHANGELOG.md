# Changelog

## v6.0.3 (2025-01-21)

### Fixed

- Updated to latest dependencies (`commander@13.1.0`).

## v6.0.2 (2025-01-03)

### Fixed

- Updated to latest dependencies (`commander@13.0.0`), which required minor
  refactoring for breaking changes.

## v6.0.1 (2024-07-02)

### Fixed

- Updated to latest dependencies (`commander@12.1.0`).
- Updated to ESLint v9, config v29, and refactor for new rules.

## v6.0.0 (2024-05-01)

### Changed

- BREAKING: Deprecated support for Node 21 (end-of-life 2024-06-01) and added
  support for Node 22 (released 2024-04-25). Compatible with all current and
  LTS releases (`^18.12.0 || ^20.9.0 || >=22.0.0`). (#42)
- BREAKING: Updated package `main` to `exports`, so only the explicitly named
  exports are available.
- Removed `escape-string-regexp` dependency (no change in functionality).

### Fixed

- Updated to latest dependencies (`ci-logger@^7.0.0`).

### Miscellaneous

- Updated Renovate config to use use new [presets](https://gitlab.com/gitlab-ci-utils/renovate-config)
  project. (#41)

## v5.0.1 (2024-02-04)

### Fixed

- Updated to latest dependencies (`commander@12.0.0`).

## v5.0.0 (2023-11-07)

### Changed

- BREAKING: Deprecated support for Node 16 (end-of-life 2023-09-11) and added
  support for Node 21 (released 2023-10-17). Compatible with all current and
  LTS releases (`^18.12.0 || >=20.0.0`). (#38, #39)

### Fixed

- Updated to latest dependencies (`commander@11.1.0`)

### Miscellaneous

- Updated package to publish to NPM with
  [provenance](https://github.blog/2023-04-19-introducing-npm-package-provenance/).

## v4.0.1 (2023-08-13)

### Fixed

- Updated to latest dependencies (`commander@11.0.0`)

## v4.0.0 (2023-06-04)

### Changed

- BREAKING: Deprecated support for Node 14 (end-of-life 2023-04-30) and 19
  (end-of-life 2023-06-01). Compatible with all current and LTS releases
  (^16.13.0 || ^18.12.0 || >=20.0.0). (#35)
- BREAKING: Added TypeScript type declarations. (#36)

### Fixed

- Updated to latest dependencies (`ci-logger@^6.0.0`, `commander@10.0.1`).

## v3.0.4 (2023-03-26)

### Fixed

- Updated to latest dependencies (`ci-logger@5.1.1`)

## v3.0.3 (2023-01-15)

### Fixed

- Updated to latest dependencies (`commander@10.0.0`)
- Fixed jsdoc `@return` statements to clarify that `undefined` is returned if no
  value is found for `findChangelog`and `getReleaseDetails`.

## v3.0.2 (2022-10-09)

### Fixed

- Updated to latest dependencies (`ci-logger@5.1.0`, `commander@9.4.1`)

### Miscellaneous

- Updated pipeline to have long-running jobs use GitLab shared medium sized runners. (#32)
- Updated CHANGELOG release names for consisistency (all now follow the format v1.2.3).

## v3.0.1 (2022-07-31)

### Fixed

- Updated to latest dependencies

## v3.0.0 (2022-05-29)

### Changed

- BREAKING: Deprecated support for Node 12 and 17 since end-of-life. Compatible with all current and LTS releases (`^14.15.0 || ^16.13.0 || >=18.0.0`). (#29, #30)

### Fixed

- Updated to latest dependencies

### Miscellaneous

- Added `prettier` and disabled `eslint` formatting rules. (#28)
- Added test coverage threshold requirements (#31)

## v2.1.4 (2022-03-26)

### Fixed

- Update to latest dependencies, including resolving CVE-2022-0235 (dev only)
- Refactored code and tests per latest linting rules

### Miscellaneous

- Updated project policy documents (CODE_OF_CONDUCT.md, CONTRIBUTING.md, SECURITY.md) (#27)

## v2.1.3 (2022-01-30)

### Fixed

- Updated to latest dependencies

## v2.1.2 (2021-11-21)

### Fixed

- Updated to latest dependencies

### Miscellaneous

- Refactored tests for per updated lint rules

## v2.1.1 (2021-09-12)

### Fixed

- Updated to latest dependencies

### Miscellaneous

- Configured [renovate](https://docs.renovatebot.com/) for dependency updates (#25)

## v2.1.0 (2021-07-06)

### Changed

- Refactored `getReleaseDetails` to simplify logic and improve efficiency (#21)

### Fixed

- Updated to latest dependencies

### Miscellaneous

- Refactored validation and CLI functions for better separation of concerns (#22)
- Added missing integration test cases (#23)

## v2.0.0 (2021-06-03)

### Changed

- BREAKING: Deprecated support for Node 10, 13 and 15 since end-of-life. Compatible with all current and LTS releases (`^12.20.0 || ^14.15.0 || >=16.0.0`). (#6, #19)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

## v1.0.9 (2021-05-11)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

### Miscellaneous

- Optimize published package to only include the minimum required files (#17)

## v1.0.8 (2021-03-15)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

### Miscellaneous

- Updated pipeline to use standard NPM package collection (#16)

## v1.0.7 (2021-02-27)

### Fixed

- Fixed issue where tags were not properly escaped, resulting in regex returning incorrect results (#15)

## v1.0.6 (2021-02-26)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities (#13)

### Miscellaneous

- Update SBOM type to application (#14)

## v1.0.5 (2020-11-25)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

### Miscellaneous

- Extracted CLI test module to new npm module [bin-tester](https://www.npmjs.com/package/bin-tester) (#8)
- Updated documentation for all exposed functions for consistency (#9)
- Updated CI pipeline to leverage simplified include syntax in GitLab 13.6 (#10) and GitLab Releaser (#11)

## v1.0.4 (2020-08-30)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

## v1.0.3 (2020-08-16)

### Fixed

- Updated to latest dependencies, including:
  - Resolving vulnerabilities
  - Updating to `commander` v6 and fixing associated test issues (#7)

## v1.0.2 (2020-05-24)

### Fixed

- Updated to latest dependencies
- Updated to latest GitLab `license_scanning` job

## v1.0.1 (2020-03-22)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

## v1.0.0 (2020-01-19)

Initial release
